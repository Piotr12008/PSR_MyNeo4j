﻿using MyNeo4j.Entity;
using Neo4j.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeo4j.Repository
{
    public class ZgloszenieRepo
    {
        private readonly IDriver _driver;

        public ZgloszenieRepo(IDriver driver)
        {
            _driver = driver;
        }
        public async Task SaveAsync(Zgloszenie zgloszenie)
        {
            int zamkniete = 0;
            if(zgloszenie.Zamknieta == true)
            {
                zamkniete = 1;
            }
            zgloszenie.Data = DateTime.Now;
            string imie = zgloszenie.Imie;
            string nazwisko = zgloszenie.Nazwisko;
            string opis = zgloszenie.Opis;
            Rodzaj rodzaj = new Rodzaj();
            rodzaj.Nazwa = zgloszenie.Rodzaj.Nazwa;
            int rodzajId = zgloszenie.Rodzaj.Id;
            var session = _driver.AsyncSession();
            try
            {
                    var result = await session.RunAsync("CREATE (a.Zgloszenie {a.imie: $imie, a.nazwisko: $nazwisko, a.opis: $opis}) RETURN id(a)", new { imie, nazwisko, opis});
                    var zglodzenieID = result.As<int>();
                    await session.RunAsync(@"MATCH (a:Person {id: $zglodzenieID}) MATCH (b:Person {id: $rodzajId}) MERGE (a)-[:KNOWS]->(b)", new { zglodzenieID, rodzajId });
            }
            catch(Exception e)
            {
                Console.WriteLine("ERROR");
            }
            
        }

        public async Task FindAll(Zgloszenie zgloszenie)
        {
            int zamkniete = 0;
            if (zgloszenie.Zamknieta == true)
            {
                zamkniete = 1;
            }
            zgloszenie.Data = DateTime.Now;
            string imie = zgloszenie.Imie;
            string nazwisko = zgloszenie.Nazwisko;
            var session = _driver.AsyncSession();
            try
            {
                return session.ReadTransactionAsync(tx =>
                {
                    var result = tx.RunAsync("MATCH (a:Zgloszenie) RETURN a.nazwisko ORDER BY a.nazwisko");
                    return result.Select(record => record[0].As<string>()).ToList();
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR");
            }

        }
    }
}
