﻿using MyNeo4j.Entity;
using Neo4j.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeo4j.Repository
{
    public class RodzajRepo
    {
        public readonly IDriver _driver;

        public RodzajRepo(IDriver driver)
        {
            _driver = driver;
        }
        public async Task SaveAsync(string nazwa)
        {
            var session = _driver.AsyncSession();
            try
            {
                await session.RunAsync("CREATE (Rodzaj {nazwa: $nazwa})", new { nazwa });
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR");
            }

        }
        public int FindOne(string name)
        {

            var session = _driver.AsyncSession();
            try
            {
                    var result = session.RunAsync("MATCH (a:Rodzaj) WHERE a.nazwa = $nazwa RETURN id(a) ", new { name });
                    return result.As<int>();
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR");
            }
            return - 1;
        }
    }
    
}
