﻿using MyNeo4j.Entity;
using MyNeo4j.Repository;
using Neo4j.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeo4j
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("start");
            PoliceProj conf = new PoliceProj("neo4j://localhost:7687", "", "");
            ZgloszenieRepo zgloszenieRepo = new ZgloszenieRepo(conf.GetDriver());
            Zgloszenie zgloszenie = new Zgloszenie();
            zgloszenie.Imie = "Piotr";
            zgloszenie.Nazwisko = "Barchan";
            zgloszenie.Opis = "Skradziony rower";
            Rodzaj rodzaj = new Rodzaj();
            rodzaj.Nazwa = "Wykroczenie";
            zgloszenie.Rodzaj = rodzaj;
            zgloszenie.Opis = "Skradziony rower";
            zgloszenieRepo.SaveAsync(zgloszenie);
            RodzajRepo rodzajRepo = new RodzajRepo(conf.GetDriver());
            rodzajRepo.SaveAsync("Wykroczenie");
            Console.WriteLine("koniec");
            Console.ReadKey();
        }
    }
    public class PoliceProj : IDisposable
    {
        private readonly IDriver _driver;

        public PoliceProj(string uri, string user, string password)
        {
            _driver = GraphDatabase.Driver(uri, AuthTokens.Basic(user, password));
        }

        public async Task PrintGreetingAsync(string message)
        {
            var session = _driver.AsyncSession();
            try
            {
                var greeting = await session.WriteTransactionAsync(async tx =>
                {
                    var result = await tx.RunAsync("CREATE (a:Greeting) " +
                                                   "SET a.message = $message " +
                                                   "RETURN a.message + ', from node ' + id(a)",
                        new { message });

                    return (await result.SingleAsync())[0].As<string>();
                });

                Console.WriteLine(greeting);
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        public async Task GetGreeting()
        {
            var session = _driver.AsyncSession();
            var greeting = await session.WriteTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (n:Greeting) RETURN n LIMIT 25");

                return (await result.SingleAsync())[0].As<string>();
            });
            Console.WriteLine(greeting);
        }

        public void Dispose()
        {
            _driver?.Dispose();
        }

        public IDriver GetDriver()
        {
            return _driver;
        }
    }
}
